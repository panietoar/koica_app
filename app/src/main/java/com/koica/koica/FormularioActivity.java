package com.koica.koica;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.UUID;

public class FormularioActivity extends AppCompatActivity {

    private static final String TAG = "Ubicacion";

    private ImageView imageMapa;
    private LocationManager locationManager;
    private Location ubicacion;
    private Bitmap bitMapMapa;

    private Uri fotoPath;
    private Uri mapaUri;

    private EditText txtNombre;
    private EditText txtCedula;
    private EditText txtFecha;
    private EditText txtZona;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);
        permisosUri();
        imageMapa = (ImageView) findViewById(R.id.imageMap);

        txtFecha = (EditText) findViewById(R.id.txtFecha);
        txtCedula = (EditText) findViewById(R.id.txtCedula);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtZona = (EditText) findViewById(R.id.txtZona);

        registrarListenerBoton();
    }

    @Override
    protected void onStart() {
        super.onStart();
        inciarTiempo();
        ubicarMapa();
        fotoPath = (Uri) getIntent().getExtras().get("fotoPath");
    }

    private void registrarListenerBoton() {
        Button botonGuardar = (Button) findViewById(R.id.btnGuardarDatos);
        final Context context = this;

        botonGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validarFormulario()) {
                    guardarInformacion();
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(context).create();
                    alertDialog.setTitle("Alerta");
                    alertDialog.setMessage("Todos los campos son obligatorios");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }
        });
    }

    private void guardarInformacion() {

        enviarCorreo();
        //TODO: Envio a REST
    }

    private void enviarCorreo() {

        String cuerpo = "";

        cuerpo += "Nombre: " + txtNombre.getText();
        cuerpo += "\nCédula: " + txtCedula.getText();
        cuerpo += "\nFecha y hora: " + txtFecha.getText();
        cuerpo += "\nZona: " + txtZona.getText();

        String asunto = "Reporte";
        String[] destino = new String[]{getResources().getString(R.string.email_prueba1)};

        Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        emailIntent.setType("plain/text");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, destino);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, asunto);
        emailIntent.putExtra(Intent.EXTRA_TEXT, cuerpo);

        ArrayList<Uri> uris = new ArrayList<>();
        uris.add(mapaUri);
        uris.add(fotoPath);
        emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);

        startActivityForResult(emailIntent, 1);

    }

    private boolean validarFormulario() {
        boolean valido = true;

        valido &= !("".equals(txtNombre.getText()));
        valido &= !("".equals(txtCedula.getText()));
        valido &= !("".equals(txtFecha.getText()));
        valido &= !("".equals(txtZona.getText()));

        return valido;
    }

    private void inciarTiempo() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd-HH:mm");
        GregorianCalendar calendar = new GregorianCalendar();
        String fecha = format.format(calendar.getTime());
        txtFecha.setText(fecha);
    }

    private void ubicarMapa() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        ubicacion = getLastKnownLocation();
        solicitarMapaConUbicacion();
    }

    private void solicitarMapaConUbicacion() {

        double latitud = 0.0;
        double longitud = 0.0;

        if (ubicacion != null) {
            latitud = ubicacion.getLatitude();
            longitud = ubicacion.getLongitude();
        }

        String urlMapa = crearUrlMapa(latitud, longitud);
        pintarMapa(urlMapa);

    }

    private class PintadorMapa extends AsyncTask<String, Void, Bitmap> {

        private Context _context;

        public PintadorMapa(Context context) {
            _context = context;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);

                bitMapMapa = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                mapaUri = getImageUri(_context, bitMapMapa);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
            return bitMapMapa;
        }

        @Override
        protected void onPostExecute(Bitmap imagen) {
            imageMapa.setImageBitmap(imagen);
        }
    }

    public void permisosUri() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String filename = "mapa_" + UUID.randomUUID();
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, filename, null);
        return Uri.parse(path);
    }

    private void pintarMapa(String urlMapa) {
        new PintadorMapa(this).execute(urlMapa);
    }

    private String crearUrlMapa(double latitud, double longitud) {
        String url = "";

        url += "http://maps.google.com/maps/api/staticmap?center=" + latitud + "," + longitud +
                "&markers=" + latitud + "," + longitud + "&zoom=13&size=400x200&sensor=false";

        return url;
    }

    private boolean isGpsLocationProviderEnabled() {
        try {
            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }

        return false;
    }

    private boolean isNetworkLocationProviderEnabled() {
        try {
            return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }

        return false;
    }

    public Location getLastKnownLocation() {

        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            obtenerPermiso();
        } else {

            Location netLoc = null;
            Location gpsLoc = null;
            if (isGpsLocationProviderEnabled()) {
                gpsLoc = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            } else if (isNetworkLocationProviderEnabled()) {
                netLoc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

            // If there are both values use the latest one
            if (gpsLoc != null && netLoc != null) {
                if (gpsLoc.getTime() > netLoc.getTime()) {
                    return gpsLoc;
                } else {
                    return netLoc;
                }
            }

            if (gpsLoc != null) {
                return gpsLoc;
            }
            if (netLoc != null) {
                return netLoc;
            }
        }
        return null;
    }

    private void obtenerPermiso() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
        }, 3);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            regresar();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 3: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    ubicacion = getLastKnownLocation();
                    solicitarMapaConUbicacion();
                } else {
                    Log.i(TAG, "No se dió permiso para localizar");
                }
                return;
            }
            case 2: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Log.i(TAG, "No se dió permiso para guardar archivos");
                }
                return;
            }
        }
    }

    private void regresar() {
        Intent inicio = new Intent(this, CamaraActivity.class);
        startActivity(inicio);
    }

}
