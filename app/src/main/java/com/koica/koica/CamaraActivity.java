package com.koica.koica;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by pabnieto on 15/07/16.
 */
public class CamaraActivity extends AppCompatActivity {

    private ImageView previewFoto;
    private Button btnGuardarFoto;
    private ImageButton btnTomarFoto;

    private Bitmap fotoBitmap;

    private Uri fotoUri;

    static final int REQUEST_IMAGE_CAPTURE = 1;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        permisosUri();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camara);
        previewFoto = (ImageView) findViewById(R.id.previewFoto);
        btnGuardarFoto = (Button) findViewById(R.id.btnGuardarFoto);
        btnTomarFoto = (ImageButton) findViewById(R.id.btnTomarFoto);
    }

    @Override
    public void onStart() {
        super.onStart();
        final Context context = this;
        btnTomarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnGuardarFoto.setVisibility(View.INVISIBLE);
                dispatchTakePictureIntent();
            }
        });

        btnGuardarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent formulario = new Intent(context, FormularioActivity.class);
                formulario.putExtra("fotoPath", fotoUri);
                Log.d("Camara", "Foto guardada");
                startActivity(formulario);
            }
        });
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;

            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                Log.e("Camara", "Error creando el archivo para la foto");
            }

            if (photoFile != null) {
                fotoUri = Uri.fromFile(photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fotoUri);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            fotoBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);

            previewFoto.setImageBitmap(fotoBitmap);
            btnGuardarFoto.setVisibility(View.VISIBLE);

        }
    }

    public void permisosUri() {
        ActivityCompat.requestPermissions(this, new String[]{
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
    }

    private String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String imageFileName = "foto_" + UUID.randomUUID();

        File storageDir = new File(Environment.getExternalStorageDirectory(), "Koica");
        storageDir.mkdirs();

        File image = new File(storageDir,
                imageFileName +
                        ".jpg"
        );
        mCurrentPhotoPath = image.getPath();
        return image;
    }

}
